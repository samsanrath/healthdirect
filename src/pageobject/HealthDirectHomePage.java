package pageobject;

import java.util.concurrent.TimeUnit;

import org.mockito.asm.tree.TryCatchBlockNode;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HealthDirectHomePage {
	public HealthDirectHomePage(WebDriver driver) {
		this.driver = driver;
	}
	static WebDriver driver;
	boolean flag = false;

	public void goToHomePage() {
		driver.get("https://www.healthdirect.gov.au/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	public void searchByKeyWord(String keyWord) {
		WebElement txtField_SearchPanel = driver
				.findElement(By
						.xpath(".//form[@id='search-form-node-desktop-mode']/input[@id='header-search']"));
		WebElement btn_Search = driver
				.findElement(By
						.xpath(".//form[@id='search-form-node-desktop-mode']/input[@id='header-search']/../button/i[@class='hda-head_menu-row-tablet-search-icon']"));
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		txtField_SearchPanel.sendKeys(keyWord);
		txtField_SearchPanel.sendKeys(Keys.ENTER);
	}

	public boolean isKeywordFound(String keyword) {
		try {
			String fromString, toString, tips = "Tips:";
			String txt_lbl_resultPaneToken = driver.findElement(
					By.xpath("//small")).getText();
			fromString = "Search results for:" + " " + txt_lbl_resultPaneToken;
			toString = "Search results for: " + "\"" + keyword + "\"";
			System.out.println(fromString);
			System.out.println(toString);
			// System.out.println(txt_lbl_resultPaneTokenUnsecc);
			if (fromString.equalsIgnoreCase(toString)) {
				flag = true;
			} else
				flag = false;

		} catch (Exception e) {
			// TODO: handle exception
		}
		return flag;
	}

	public void tearDown() {
		driver.close();
		driver.quit();
	}

	public void goToHome() {

		WebElement btn_home = driver.findElement(By
				.xpath("//a/img[@alt='healthdirect australia logo']"));
		btn_home.click();
		String title = driver.getTitle();
		System.out.println(title);
	}

	public boolean isBrowserClosed() {
		try {
			flag = false;
			if (driver.toString().contains("null")) {
				flag = true;
			} else
				flag = false;
		} catch (Exception e) {

		}
		return flag;
	}

	public boolean isKeywordNotFound(String keyword) {
		try {
			flag = false;
			String tips = "Tips:";
			String lbl_tip = driver.findElement(By.xpath("//div/h2")).getText();
			System.out.println(lbl_tip);
			if (lbl_tip.equalsIgnoreCase(tips)) {
				flag = true;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return flag;
	}

}
