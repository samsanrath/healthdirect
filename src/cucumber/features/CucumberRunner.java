package cucumber.features;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;


@RunWith(Cucumber.class)
@CucumberOptions(
		plugin = {"json-pretty", "html:target/cucumber"},
		features = {"Feature"},
		glue={"src/stepDefinition"},
		tags=
			{
				"@Regression","@Acceptance","@Functional"
				//"@Acceptance"
			}
		)

public class CucumberRunner {

}
