package stepDefinition;

import junit.framework.Assert;
import pageobject.HealthDirectHomePage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SearchFunctionalityTest extends TestBase{
	String Keyword;
	@Given("^that I'm a user who is in healthdirect home page$")
	public void that_I_m_a_user_who_is_in_healthdirect_home_page() throws Throwable {
		HealthDirectHomePage healthDirectHomePage=new HealthDirectHomePage(driver);
		healthDirectHomePage.goToHomePage();
		//Assert.assertTrue("");
	}

	@When("^I enter keyword as \"([^\"]*)\"and hit enter$")
	public void i_enter_keyword_as_and_hit_enter(String arg1) throws Throwable {
		HealthDirectHomePage healthDirectHomePage=new HealthDirectHomePage(driver);
		healthDirectHomePage.searchByKeyWord(arg1);
		Keyword=arg1;
	}

	@Then("^I should be able to see search results$")
	public void i_should_be_able_to_see_search_results() throws Throwable {
		HealthDirectHomePage healthDirectHomePage=new HealthDirectHomePage(driver);
		Assert.assertTrue(healthDirectHomePage.isKeywordFound(Keyword));
	}
	@Given("^I'm in healthdirect home page$")
	public void i_m_in_healthdirect_home_page() throws Throwable {
		HealthDirectHomePage healthDirectHomePage=new HealthDirectHomePage(driver);
		healthDirectHomePage.goToHome();
	}

	@When("^I close the healthdirect browser window$")
	public void i_close_the_healthdirect_browser_window() throws Throwable {
		HealthDirectHomePage healthDirectHomePage=new HealthDirectHomePage(driver);
		healthDirectHomePage.tearDown();
	}

	@Then("^healthdirect Browser window should be closed$")
	public void healthdirect_Browser_window_should_be_closed() throws Throwable {
		HealthDirectHomePage healthDirectHomePage=new HealthDirectHomePage(driver);
		Assert.assertTrue(healthDirectHomePage.isBrowserClosed());
	}
	
	@Then("^I should be able to see search suggesions$")
	public void i_should_be_able_to_see_search_suggesions() throws Throwable {
		HealthDirectHomePage healthDirectHomePage=new HealthDirectHomePage(driver);
		Assert.assertTrue(healthDirectHomePage.isKeywordNotFound(Keyword));
	}

}
